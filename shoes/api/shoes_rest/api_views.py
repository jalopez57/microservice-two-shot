from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import BinVO, Shoe
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size","import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer","model_name", "color", "image", "bin"]

    encoders={
        "bin": BinVOEncoder()
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image",
        ]
    def get_extra_data(self, o):
        return {
            "closet_name": o.bin.closet_name,
            "bin_number": o.bin.bin_number,
            "bin_size": o.bin.bin_size
        }

@require_http_methods(["GET", "POST"])
def api_shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder)

    else: # THIS IS MY POST METHOD
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Number"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_shows_shoes(request, pk):
    if request.method =="GET":
        shoe=Shoe.objects.get(id=pk)
        return JsonResponse(
        shoe, encoder=ShoeDetailEncoder,
        safe=False
        )
    elif request.method =="DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count >0 })
    else:
        content= json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(import_href=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin number"},
                status=400
            )

        Shoe.objects.filter(id=pk).update(**content)

        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe,encoder=ShoeDetailEncoder,
        safe=False,
        )
