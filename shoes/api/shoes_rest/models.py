from django.db import models
from django.urls import reverse


# Create your models here.


class BinVO(models.Model):
    """
    The bin is the value object of the SHOE API
    """

    closet_name = models.CharField(max_length=100, null=True)
    bin_number =models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    image =models.URLField(null=True)
    bin = models.ForeignKey(BinVO,
    related_name ="shoe_bin",
     on_delete=models.PROTECT)

    # def __str__(self):
    #     return self.model_name

    # def get_api_url(self):
    #     return reverse("api_shoe_list", kwargs={"pk": self.id})
