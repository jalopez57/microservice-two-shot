from django.urls import path
from .api_views import api_shoe_list, api_shows_shoes


urlpatterns = [
    path('shoes/', api_shoe_list, name="shoe_list"),
    path('shoes/<int:pk>/', api_shows_shoes, name="shoe_detail"),

    ]
