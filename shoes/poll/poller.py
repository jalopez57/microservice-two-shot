import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO


def get_bin():
    response = requests.get(" http://wardrobe-api:8000/api/bins/")
    # print(response)
    content = json.loads(response.content)
    # print(content)
    for bin in content["bins"]:
        print(content)
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
            "closet_name": bin["closet_name"],
            "bin_number": bin["bin_number"],
            "bin_size": bin["bin_size"]
            },
        )
    # print(BinVO.objects.get(import_href=bin["href"]))

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
           get_bin()
           print('THIS IS A BIN---------')
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
