# Generated by Django 4.0.3 on 2022-10-20 17:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_remove_hat_url_hat_picture_url_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locationvo',
            old_name='section_name',
            new_name='section_number',
        ),
    ]
