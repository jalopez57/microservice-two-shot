from django.urls import path, include
from .api_views import api_list_hats, api_list_location, api_shows_hats

urlpatterns = [
    path('hats/', api_list_hats, name="api_list_hats"),
    path('locations/', api_list_location, name="api_list_Hat"),
    path('hats/<int:pk>/', api_shows_hats, name="details_hat"),
    path('hats/<int:pk/', api_shows_hats, name='delete_hat'),
    ]
